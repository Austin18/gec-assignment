#ifndef GAMEOVER_SCREEN_H
#define GAMEOVER_SCREEN_H

#include <vector>
#include <string>
#include "GameScreen.h"
#include "GameScreenManager.h"
using namespace std;

class GameOverScreen : public GameScreen {
public:
	GameOverScreen(GameScreenManager* manager);
	~GameOverScreen() {};
	void Render();
	void Update(float deltaTime, SDL_Event e);

protected:

	void HandleInput(SDL_Event e);
	vector<string> mInstructions;



};
#endif //GAMEOVER_SCREEN_H



