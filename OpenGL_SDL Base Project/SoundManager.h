#ifndef SOUND_MANAGER_H
#define SOUND_MANAGER_H

#include <SDL.h>
#include <SDL_mixer.h>
#include <stdio.h>
#include <string>

class SoundManager
{
	private:
		SoundManager();
		static SoundManager * mInstance;
		Mix_Chunk *mBounce;
		Mix_Chunk *mScream;
		Mix_Chunk *mApplause;
		Mix_Chunk *mCannon;
		Mix_Chunk *mBoo;
		bool mSuccess;

	public:
		static SoundManager * GetInstance();
		void PlayBounce();
		void PlayScream();
		void PlayApplause();
		void PlayCannon();
		void PlayBoo();
		bool LoadSounds();
	


};



#endif // !SOUND_MANAGER_H

