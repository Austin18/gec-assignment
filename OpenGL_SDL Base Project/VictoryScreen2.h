#ifndef VICTORY_SCREEN2_H
#define VICTORY_SCREEN2_H

#include <vector>
#include <string>
#include "GameScreen.h"
#include "GameScreenManager.h"
using namespace std;

class VictoryScreen2 : public GameScreen {
public:
	VictoryScreen2(GameScreenManager* manager);
	~VictoryScreen2() {};
	void Render();
	void Update(float deltaTime, SDL_Event e);

protected:

	void HandleInput(SDL_Event e);
	vector<string> mInstructions;



};
#endif //VICTORY_SCREEN2_H





