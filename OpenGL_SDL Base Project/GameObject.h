#ifndef _GAMEOBJECT_H
#define _GAMEOBJECT_H
#include "commons.h"
#include "Object3DS.h"


class GameObject {

public:
	GameObject(Object3DS* mesh, Vector3D startposition, Vector3D startrotation);
	~GameObject();


	virtual void Render();
	virtual void Update(float DeltaTime);
	
protected:
	Object3DS* m_meshObject;
	Vector3D m_position;
	Vector3D m_rotation;
};






#endif //_GAMEOBJECT_H
