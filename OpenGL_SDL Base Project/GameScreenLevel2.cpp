#include "GameScreenLevel2.h"
#include <time.h>
#include <windows.h>
#include <GL\gl.h>
#include <GL\glu.h>
#include "../gl/glut.h"
#include "Constants.h"
#include "Texture2D.h"
#include "HelicopterGameobject.h"
#include "CourtGameobject.h"
#include <iostream>
#include "Collisions.h"
#include "ProjectileGameobject.h"
#include "Score.h"
#include "SoundManager.h"
#include "TargetGameObject.h" 


using namespace::std;

//--------------------------------------------------------------------------------------------------

GameScreenLevel2::GameScreenLevel2(GameScreenManager* manager) : GameScreen(manager)
{
	srand(time(NULL));

	glEnable(GL_DEPTH_TEST);

	glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	float aspect = (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT;
	gluPerspective(60.0f, aspect, 0.1f, 1000.0f);

	glMatrixMode(GL_MODELVIEW);

	glEnable(GL_CULL_FACE);								//Stop calculation of inside faces
	glEnable(GL_DEPTH_TEST);							//Hidden surface removal

														//clear background colour.
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glEnable(GL_TEXTURE_2D);







	//set some parameters so it renders correctly


	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	mCam = new Camera();
	mCam->GetInstance();


	SetUpLevel();

	mCurrentTime = 30;
}

//--------------------------------------------------------------------------------------------------

GameScreenLevel2::~GameScreenLevel2()
{
}

//--------------------------------------------------------------------------------------------------


void GameScreenLevel2::SetLight() {
	lighting light = {
		{ 0.2f, 0.2f, 0.2f, 1.0f },
		{ 0.7f, 0.7f, 0.7f, 1.0f },
		{ 0.5f, 0.5f, 0.5f, 1.0f }
	};
	// position of the light in homogeneous coordinates (x, y, z, w)
	// w should be 0 for directional lights, and 1 for spotlights
	float light_pos[] = { 1.0f, 1.0f, 1.0f, 0.0f };

	glLightfv(GL_LIGHT0, GL_AMBIENT, light.ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light.diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light.specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
}

void GameScreenLevel2::SetMaterial()
{
	material material = {
		{ 0.80f, 0.05f, 0.05f, 1.0f },
		{ 0.80f, 0.05f, 0.05f, 1.0f },
		{ 1.0f, 1.0f, 1.0f, 1.0f },
		100.0f
	};

	glMaterialfv(GL_FRONT, GL_AMBIENT, material.ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material.diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material.specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material.shininess);
}


void GameScreenLevel2::Render()
{
	//Clear the screen.
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	for (int i = 0; i < gameObjects.size(); i++)
	{
		gameObjects[i]->Render();
	}

	for (int i = 0; i < ProjectilegameObjects.size(); i++)
	{
		ProjectilegameObjects[i]->Render();
	}
	
	for (int j = 0; j < TargetgameObjects.size(); j++)
	{
		TargetgameObjects[j]->Render();
	
	}
	

	//RenderCharacter
	charactergameObjects[0]->Render();

	mCam->Render();

	glColor3f(0.0, 1.0, 0.0);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, 100, 0, 100);
	OutputLine(17, 95, StringStream.str());
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

}

//--------------------------------------------------------------------------------------------------

void GameScreenLevel2::Update(float deltaTime, SDL_Event e)
{
		mCurrentTime -= deltaTime;
	cout << mCurrentTime << endl;

	StringStream.str("");
	StringStream.clear();
	StringStream << "Number Of Hits: " << mNumOfHits << "  " << endl << "Time:  " << (int)mCurrentTime << endl;

	mNumOfHits = ProjectilegameObjects[0]->numOfHits;
	
	mCam->Update(deltaTime, e);
	
	//Update GameObjects
	charactergameObjects[0]->Update(deltaTime, e);
		
	ProjectilegameObjects[0]->Update(deltaTime, charactergameObjects[0]->Getposition());
		
	for (int i = 0; i < TargetgameObjects.size(); i++)
	{
		TargetgameObjects[i]->Update(deltaTime, Vector3D(3, 1, 0));
	}
	

	//Check if character is firing and fire projectile
	if (charactergameObjects[0]->isFiring == true)
	{
		ProjectilegameObjects[0]->setFired();
	}


	//Check for Collisions between projectile and target
	for (int i = 0; i < TargetgameObjects.size() ; i++) {
		for (int j = i + 1; j < TargetgameObjects.size(); j++) {
			Collision::SphereSphereCollision(TargetgameObjects[i]->GetBoundingSphere(), ProjectilegameObjects[0]->GetBoundingSphere());
		}
	}

	//Check Win && Loss
	if (mNumOfHits >= 20)
	{
		SoundManager::GetInstance()->PlayApplause();
		Score::GetInstance()->IncrementValue((100 + (int)mCurrentTime));
		ScreenManager->ChangeScreen(SCREEN_VICTORY2);
	}
	else if (mCurrentTime <= 0)
	{
		ScreenManager->ChangeScreen(SCREEN_GAMEOVER);
	}
	
		
		
	
}




void GameScreenLevel2::SetUpLevel()
{


	m_Target = new Object3DS("Denzil.3ds", "DenzilDiffuse.TGA");
	m_Heli = new Object3DS("Helicopter.3ds", "BallTexture.TGA");
	m_Projectile = new Object3DS("Ball.3ds", "BallTexture.TGA");
	m_Court = new Object3DS("Court.3ds", "SandTexture.TGA");


	HelicopterGameobject* HeliObj = new HelicopterGameobject(m_Heli, Vector3D(0, -20, 0), Vector3D(0, 0, -90));
	ProjectileGameObject* projectileObj = new ProjectileGameObject(m_Projectile, Vector3D(-15, -13, 0), Vector3D(0, 0, 0));
	CourtGameObject* courtobj = new CourtGameObject(m_Court, Vector3D(0, 5, 0), Vector3D(0, 0, 0));
	CourtGameObject* courtobj2 = new CourtGameObject(m_Court, Vector3D(40, 5, 0), Vector3D(0, 0, 180));

	for (int i = 0; i < 30; i++)
	{
		TargetGameObject* targetObj = new TargetGameObject(m_Target, Vector3D(3, 1, 0), Vector3D(0, 0, 180));
		TargetgameObjects.push_back(targetObj);
	}

	ProjectilegameObjects.push_back(projectileObj);
	charactergameObjects.push_back(HeliObj);

	gameObjects.push_back(courtobj);
	gameObjects.push_back(courtobj2);
}


//--------------------------------------------------------------------------------------------------