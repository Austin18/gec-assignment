#include "BallGameobject.h"
#include <iostream>
#include "../gl/glut.h"
#include "Collisions.h"
#include "DenzilGameobject.h"
#include <Windows.h>
#include "SoundManager.h"
using namespace std;

BallGameObject::BallGameObject(Object3DS* mesh, Vector3D startposition, Vector3D startrotation) : GameObject(mesh, startposition, startrotation)
{
	m_position.x = startposition.x * (float)rand() / (RAND_MAX)* (10 - -15) + -15;
	m_position.y = startposition.y * (float)rand() / (RAND_MAX)* (15 - -5) + -5;
	m_position.z = startposition.z * (float)rand() / (RAND_MAX)-5;


	scale = 0.7;
	boundingSphere = new Sphere(m_position, scale*1.4);

	velocity.x = 5000 * (float)rand() / ((RAND_MAX) * 100000) - .005;
	velocity.y = 5000 * (float)rand() / ((RAND_MAX) * 100000) - .005;
	velocity.z = 5000 * (float)rand() / ((RAND_MAX) * 1000000) - .005;
	
	acceleration.x = 0;
	acceleration.y = 0;
	acceleration.z = -50.0;
	
	
	cout << velocity.x<<"," << velocity.y <<","<< velocity.z<< endl;
}

BallGameObject::~BallGameObject()
{}

void BallGameObject::Render()
{
	if (m_meshObject != nullptr)
	{

		

		GameObject::Render();
		
		
	}
	else
	{
		glDisable(GL_LIGHTING);
		glBindTexture(GL_TEXTURE_2D, 0);
		glColor3f(1.0f, 0.5f, 0.0f);
		glPushMatrix();
		glTranslatef(m_position.x, m_position.y, m_position.z);
		glutSolidSphere(.5f, 10, 10);
		glPopMatrix();
	}
}

void BallGameObject::Update(float DeltaTime,Vector3D newpos)
{
	boundingSphere->SetCollided(false);
	if (isoffset)
	{
		
		m_position.x = newpos.x - offset;
		m_position.y = newpos.y;
		m_position.z = newpos.z;
	}
	else {
		m_position += velocity*DeltaTime + acceleration * (0.5 * DeltaTime * DeltaTime);
		velocity += acceleration * DeltaTime;

		velocity *= 0.98;

		if (m_position.x > 40 || m_position.x < -40)
			velocity.x *= -1;

		if (m_position.y > 12 || m_position.y < -12)
			velocity.y *= -1.5;

		if (m_position.z > 40 || m_position.z < 0)
		{
			velocity.z *= -1.5;

			SoundManager::GetInstance()->PlayBounce();

		}

	}
		boundingSphere->Update(m_position);
	
	
}

Sphere * BallGameObject::GetBoundingSphere() 
{
	return boundingSphere;
}



void BallGameObject::Offsetball(Vector3D newpos)
{
		velocity.x = 0.0;
		velocity.y = 0.0;
		velocity.z = 0.0;
		acceleration.z = 0.0;
		
		isoffset = true;
		
	
}

bool BallGameObject::collided()
{
	if (boundingSphere->GetCollided())
	{
		return true;
	}
	

}


