#ifndef _OBJECT_3DS_H
#define _OBJECT_3DS_H

#include "Commons.h"
#include <string>
using std::string;

class Object3DS
{

public:
	Object3DS(string modelFileName, string textureFilename);
	~Object3DS(){};

	 void update(float deltaTime);
	 void render();

	//Load 3DS file
	void loadModel();


		//Load texture for this model
	 void loadTexture(const char* filename);

private:

	char fileName[20];
	char textureName[20];

	obj_type object;

};

#endif // _OBJECT_3DS_H