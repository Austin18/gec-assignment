#include "TargetGameObject.h"
#include <iostream>
#include "../gl/glut.h"
#include "Collisions.h"
#include "DenzilGameobject.h"
#include <Windows.h>
#include "SoundManager.h"
using namespace std;

TargetGameObject::TargetGameObject(Object3DS* mesh, Vector3D startposition, Vector3D startrotation) : GameObject(mesh, startposition, startrotation)
{
	m_position.x = startposition.x * (float)rand() / (RAND_MAX)* (10 - -15) + -15;
	m_position.y = startposition.y * (float)rand() / (RAND_MAX)* (15 - -5) + -5;
	m_position.z = startposition.z;
	

	

	scale = 0.7;
	boundingSphere = new Sphere(m_position, scale*1.4);


	cout << velocity.x << "," << velocity.y << "," << velocity.z << endl;
}

TargetGameObject::~TargetGameObject()
{}

void TargetGameObject::Render()
{
	if (m_meshObject != nullptr)
	{

		GameObject::Render();


	}
	else
	{
		glDisable(GL_LIGHTING);
		glBindTexture(GL_TEXTURE_2D, 0);
		glColor3f(1.0f, 0.5f, 0.0f);
		glPushMatrix();
		glTranslatef(m_position.x, m_position.y, m_position.z);
		glutSolidSphere(.5f, 10, 10);
		glPopMatrix();
	}
}

void TargetGameObject::Update(float DeltaTime, Vector3D newpos)
{

	if (boundingSphere->GetCollided())
	{

		m_position.x = newpos.x * (float)rand() / (RAND_MAX)* (10 - -15) + -15;
		m_position.y = newpos.y * (float)rand() / (RAND_MAX)* (15 - -5) + -5;
		
		
	}

	boundingSphere->SetCollided(false);
	boundingSphere->Update(m_position);


}

Sphere * TargetGameObject::GetBoundingSphere()
{
	return boundingSphere;
}


bool TargetGameObject::collided()
{
	if (boundingSphere->GetCollided())
	{
		return true;
	}


}




