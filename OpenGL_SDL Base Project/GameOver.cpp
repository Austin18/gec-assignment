#include "GameOver.h"
#include "GameScreenManager.h"
#include "SoundManager.h"

GameOverScreen::GameOverScreen(GameScreenManager* manager) : GameScreen(manager) {

	mInstructions.push_back("Game Over");
	mInstructions.push_back("Hit enter to start");
	SoundManager::GetInstance()->PlayBoo();
}

void GameOverScreen::Render() {
	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, 100, 0, 100);
	//Clear the screen.
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	for (int j = 0; j < mInstructions.size(); j++) {
		glColor3f(0.0, 0.0, 0.0);
		OutputLine(20, 70 - 10 * j - 10 * j, mInstructions[j]);
	}

	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}

void GameOverScreen::Update(float deltaTime, SDL_Event e) {
	HandleInput(e);
}

void GameOverScreen::HandleInput(SDL_Event e) {
	switch (e.type)
	{
	case SDL_KEYUP:
		switch (e.key.keysym.sym) {
		case SDLK_RETURN:
			ScreenManager->ChangeScreen(SCREEN_MENU);
			break;
		}
	}
}



