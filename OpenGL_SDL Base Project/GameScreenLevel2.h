#ifndef _GAMESCREENLEVEL2_H
#define _GAMESCREENLEVEL2_H

#include <SDL.h>
#include "GameScreen.h"
#include "Object3DS.h"
#include "Camera.h"
#include <string>
#include <sstream>
#include <vector>
#include "GameObject.h"
#include "HelicopterGameobject.h"
#include "TargetGameobject.h"
#include "ProjectileGameObject.h"


class GameScreenLevel2 : GameScreen
{
	//--------------------------------------------------------------------------------------------------
public:
	GameScreenLevel2(GameScreenManager* manager);
	~GameScreenLevel2();

	void Render();
	void Update(float deltaTime, SDL_Event e);
	void SetLight();
	void SetMaterial();
	void SetUpLevel();
	float Angle;
	float offset = 5.0;
	float mTime;
	stringstream StringStream;
	int mNumOfHits = 0;






	//--------------------------------------------------------------------------------------------------
private:

	float mCurrentTime = 0;
	vector<ProjectileGameObject*> ProjectilegameObjects;
	vector<TargetGameObject*> TargetgameObjects;
	vector<HelicopterGameobject*>charactergameObjects;
	vector<GameObject*> gameObjects;
	Object3DS* m_Heli;
	Object3DS* m_Target;
	Object3DS* m_Projectile;
	Object3DS* m_Court;
	Camera* mCam;
};


#endif //_GAMESCREENLEVEL2_H
