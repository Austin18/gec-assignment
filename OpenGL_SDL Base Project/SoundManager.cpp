#include "SoundManager.h"


SoundManager * SoundManager::mInstance = 0;

SoundManager::SoundManager()
{
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
	{
		printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
		mSuccess = false;
	}

	//Initialize SDL_mixer
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
	{
		printf("SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError());
		mSuccess = false;
	}

	LoadSounds();
}

SoundManager * SoundManager::GetInstance() {
	if (!mInstance) {
		mInstance = new SoundManager();
	}
	return mInstance;
}


bool SoundManager::LoadSounds()
{
	mBounce = Mix_LoadWAV("Bounce.wav");
	if (mBounce == NULL)
	{
		printf("Failed to load Bounce sound effect! SDL_mixer Error: %s\n", Mix_GetError());
		mSuccess = false;
	}

	mScream = Mix_LoadWAV("Scream.wav");
	if (mScream == NULL)
	{
		printf("Failed to load Scream sound effect! SDL_mixer Error: %s\n", Mix_GetError());
		mSuccess = false;
	}

	mApplause = Mix_LoadWAV("Applause.wav");
	if (mApplause == NULL)
	{
		printf("Failed to load Applause sound effect! SDL_mixer Error: %s\n", Mix_GetError());
		mSuccess = false;
	}

	mCannon = Mix_LoadWAV("Cannon.wav");
	if (mCannon == NULL)
	{
		printf("Failed to load Cannon sound effect! SDL_mixer Error: %s\n", Mix_GetError());
		mSuccess = false;
	}
	mBoo = Mix_LoadWAV("Boo.wav");
	if (mCannon == NULL)
	{
		printf("Failed to load Boo sound effect! SDL_mixer Error: %s\n", Mix_GetError());
		mSuccess = false;
	}

	return mSuccess;
}

void SoundManager::PlayBounce()
{
	Mix_PlayChannel(-1, mBounce, 0);
	
}

void SoundManager::PlayScream()
{
	Mix_PlayChannel(1, mScream, 0);

}

void SoundManager::PlayApplause()
{

	Mix_PlayChannel(2, mApplause, 0);
}

void SoundManager::PlayCannon()
{
	Mix_PlayChannel(2, mCannon, 0);
}

void SoundManager::PlayBoo()
{
	Mix_PlayChannel(2, mBoo, 0);
}

