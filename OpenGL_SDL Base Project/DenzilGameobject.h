#ifndef _DENZIL_GAMEOBJECT_H
#define _DENZIL_GAMEOBJECT_H

#include "Commons.h"
#include "GameObject.h"
#include <SDL.h>
class Sphere;
class DenzilGameObject : public GameObject
{
public:
	DenzilGameObject(Object3DS* mesh, Vector3D startposition, Vector3D startrotation);
	~DenzilGameObject();

	void Update(float deltaTime, SDL_Event e);
	void Render();
	PLAYER_MOVEMENT movement;
	void ForwardBack(float deltaTime);
	void RightLeft(float deltaTime);
	Sphere * GetBoundingSphere();
	Vector3D Getposition();
	void Updatehits();
	int numOfHits = 0;
	bool Win;
	bool Dead;

	bool MovingForward = false;
	bool MovingBack = false;
	bool MovingRight = false;
	bool MovingLeft = false;
	bool LookingUp = false;
	bool LookingDown = false;
	bool LookingRight = false;
	bool LookingLeft = false;

private:
	Vector3D forward = Vector3D();
	Vector3D up = Vector3D();
	Vector3D right = Vector3D();


	//horizontal angle :toward -Z
	float yaw = 3.14f;

	

	Sphere * boundingSphere;
	




};
#endif //_DENZIL_GAMEOBJECT_H

