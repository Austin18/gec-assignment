#include "InstructionsScreen1.h"
#include "GameScreenManager.h"

InstructionsScreen1::InstructionsScreen1(GameScreenManager* manager) : GameScreen(manager) {

	mInstructions.push_back("Avoid the balls and get to the square at the top right of the court");
	mInstructions.push_back("Controls: WASD to move");
	mInstructions.push_back("Hit enter to start");
}

void InstructionsScreen1::Render() {
	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, 100, 0, 100);
	//Clear the screen.
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	for (int j = 0; j < mInstructions.size(); j++) {
		glColor3f(0.0, 0.0, 0.0);
		OutputLine(20, 70 - 10 * j - 10 * j, mInstructions[j]);
	}

	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}

void InstructionsScreen1::Update(float deltaTime, SDL_Event e) {
	HandleInput(e);
}

void InstructionsScreen1::HandleInput(SDL_Event e) {
	switch (e.type)
	{
	case SDL_KEYUP:
		switch (e.key.keysym.sym) {
		case SDLK_RETURN:
			ScreenManager->ChangeScreen(SCREEN_LEVEL1);
			break;
		}
	}
}



