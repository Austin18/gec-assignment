#ifndef _BALL_GAMEOBJECT_H
#define _BALL_GAMEOBJECT_H

#include "Commons.h"
#include "GameObject.h"

class Sphere;
class BallGameObject : public GameObject
{
public:
	BallGameObject(Object3DS* mesh, Vector3D startposition, Vector3D startrotation);
	~BallGameObject();

	void Update(float deltaTime, Vector3D newpos);
	void Render();
	Sphere * GetBoundingSphere();
	float offset = 5;
	void Offsetball(Vector3D newpos);
	bool collided();
	bool isoffset = false;
	
private:
	Object3DS* m_meshObject;
	Sphere * boundingSphere;
	Vector3D velocity;
	float scale;
	Vector3D acceleration;
};




#endif //_BALL_GAMEOBJECT_H

