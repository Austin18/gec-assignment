#include "DenzilGameobject.h"
#include "Collisions.h"
#include "Constants.h"
#include <math.h> 
#include "../gl/glut.h"
#include <iostream>
#include "SoundManager.h"
using namespace std;



DenzilGameObject::DenzilGameObject(Object3DS* mesh, Vector3D startposition, Vector3D startrotation) : GameObject(mesh, startposition, startrotation)
{
	float scale = 0.7;
	boundingSphere = new Sphere(m_position, scale*1.4); 
}

DenzilGameObject::~DenzilGameObject()
{}

void DenzilGameObject::Update(float deltaTime, SDL_Event e)
{
	if (boundingSphere->GetCollided())
	{
		Updatehits();
		cout << numOfHits << endl;
	}
	boundingSphere->SetCollided(false);

	if (m_position.x > 55) 
		m_position.x += -1;

	if (m_position.x < -20)
		m_position.x += 1;

	if (m_position.y > 14)
		m_position.y += -1;

	if(m_position.y < -12)
		m_position.y += 1;

	if (m_position.x > 52 && m_position.y > 12)
	{
		Win = true;
		cout << Win << endl;
	}
	

	
	// Forward Vector: Spherical coordinates to Cartesian coordinates conversion (also known as the �look� direction)
	// Right vector
	right = Vector3D(
		sin(yaw - 3.14f / 2.0f),
		0,
		cos(yaw - 3.14f / 2.0f));
	// Up vector : perpendicular to both forward and right, calculate using the cross product
	up = Vector3D((right.y*forward.z) - (right.z*forward.y),
		(right.z*forward.x) - (right.x*forward.z),
		(right.x*forward.y) - (right.y*forward.x));

	//Managing Movement Functions

	switch (movement)
	{
	case cMovingForward:
		ForwardBack(deltaTime);
		break;

	case cMovingBack:
		ForwardBack(deltaTime);
		break;

	case cMovingRight:
		RightLeft(deltaTime);
		break;

	case cMovingLeft:
		RightLeft(deltaTime);
		break;
	case cStop:
		break;
	default:
		break;
	}

	//Inputs from Keyboard
	switch (e.type)
	{
	case SDL_KEYDOWN:
		switch (e.key.keysym.sym)
		{
		case SDLK_w:
			movement = cMovingForward;
			MovingForward = true;
			break;

		case SDLK_s:
			movement = cMovingBack;
			MovingBack = true;
			break;

		case SDLK_d:
			movement = cMovingRight;
			MovingRight = true;

			break;

		case SDLK_a:
			movement = cMovingLeft;
			MovingLeft = true;
			break;
		default:
			break;
		}
		break;

	case SDL_KEYUP:
		switch (e.key.keysym.sym)
		{
		case SDLK_w:
			movement = cStop;
			MovingForward = false;
			break;

		case SDLK_s:
			movement = cStop;
			MovingBack = false;
			break;

		case SDLK_d:
			movement = cStop;
			MovingRight = false;
			break;

		case SDLK_a:
			movement = cStop;
			MovingLeft = false;
			break;
		default:
			break;
		}

	}
	

	

	boundingSphere->Update(m_position);

}

void DenzilGameObject::Render()
{
	
	GameObject::Render();
}


void DenzilGameObject::ForwardBack(float deltaTime)
{
	if (MovingForward)
	{
		m_position.y += (DENZIL_MOVEMENTSPEED * deltaTime);
	}
	else if (MovingBack)
	{
		m_position.y -= (DENZIL_MOVEMENTSPEED * deltaTime);
	}
}


void DenzilGameObject::RightLeft(float deltaTime)
{
	if (MovingLeft)
	{
		m_position -= right * (DENZIL_MOVEMENTSPEED * deltaTime);
	}
	else if (MovingRight)
	{
		m_position += right * (DENZIL_MOVEMENTSPEED * deltaTime);
	}
}

Sphere * DenzilGameObject::GetBoundingSphere() 
{
	return boundingSphere;
}

Vector3D DenzilGameObject::Getposition()
{
	return m_position;
}

void DenzilGameObject::Updatehits()
{
	numOfHits++;

	if (numOfHits >= 4)
	{
		SoundManager::GetInstance()->PlayScream();
		Dead = true;
		cout << Dead << endl;
	}
}

