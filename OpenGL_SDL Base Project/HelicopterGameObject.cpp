#include "HelicopterGameobject.h"
#include "Collisions.h"
#include "Constants.h"
#include <math.h> 
#include "../gl/glut.h"
#include <iostream>
#include "SoundManager.h"
using namespace std;



HelicopterGameobject::HelicopterGameobject(Object3DS* mesh, Vector3D startposition, Vector3D startrotation) : GameObject(mesh, startposition, startrotation)
{
	float scale = 0.7;
	boundingSphere = new Sphere(m_position, scale*1.4);
}

HelicopterGameobject::~HelicopterGameobject()
{}

void HelicopterGameobject::Update(float deltaTime, SDL_Event e)
{
		// Forward Vector: Spherical coordinates to Cartesian coordinates conversion (also known as the �look� direction)
	// Right vector
	right = Vector3D(
		sin(yaw - 3.14f / 2.0f),
		0,
		cos(yaw - 3.14f / 2.0f));
	// Up vector : perpendicular to both forward and right, calculate using the cross product
	up = Vector3D((right.y*forward.z) - (right.z*forward.y),
		(right.z*forward.x) - (right.x*forward.z),
		(right.x*forward.y) - (right.y*forward.x));

	//Managing Movement Functions

	switch (movement)
	{
	
	case cMovingRight:
		RightLeft(deltaTime);
		break;

	case cMovingLeft:
		RightLeft(deltaTime);
		break;
	case cStop:
		break;
	default:
		break;
	}

	//Inputs from Keyboard
	switch (e.type)
	{
	case SDL_KEYDOWN:
		switch (e.key.keysym.sym)
		{
	
		case SDLK_d:
			movement = cMovingRight;
			MovingRight = true;

			break;

		case SDLK_a:
			movement = cMovingLeft;
			MovingLeft = true;
			break;

		case SDLK_SPACE:
			SoundManager::GetInstance()->PlayCannon();
			isFiring = true;
			break;

		default:
			break;
		}
		break;

	case SDL_KEYUP:
		switch (e.key.keysym.sym)
		{
		
		case SDLK_d:
			movement = cStop;
			MovingRight = false;
			break;

		case SDLK_a:
			movement = cStop;
			MovingLeft = false;
			break;

		case SDLK_SPACE:
			isFiring = false;
			break;

		default:
			break;
		}

	}




	boundingSphere->Update(m_position);

}

void HelicopterGameobject::Render()
{

	GameObject::Render();
}



void HelicopterGameobject::RightLeft(float deltaTime)
{
	if (MovingLeft)
	{
		m_position -= right * (DENZIL_MOVEMENTSPEED * deltaTime);
	}
	else if (MovingRight)
	{
		m_position += right * (DENZIL_MOVEMENTSPEED * deltaTime);
	}
}

Sphere * HelicopterGameobject::GetBoundingSphere()
{
	return boundingSphere;
}

Vector3D HelicopterGameobject::Getposition()
{
	return m_position;
}

void HelicopterGameobject::Updatehits()
{
	numOfHits++;

	if (numOfHits >= 20)
	{
		//SoundManager::GetInstance()->PlayScream();
		Win = true;

	}
}

