#include "GameScreenLevel1.h"
#include <time.h>
#include <windows.h>
#include <GL\gl.h>
#include <GL\glu.h>
#include "../gl/glut.h"
#include "Constants.h"
#include "Texture2D.h"
#include "BallGameobject.h"
#include "CourtGameobject.h"
#include <iostream>
#include "Collisions.h"
#include "DenzilGameobject.h"
#include "Score.h"
#include "SoundManager.h"


using namespace::std;

//--------------------------------------------------------------------------------------------------

GameScreenLevel1::GameScreenLevel1(GameScreenManager* manager) : GameScreen(manager)
{
	srand(time(NULL));

	glEnable(GL_DEPTH_TEST);

	glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	float aspect = (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT;
	gluPerspective(60.0f, aspect, 0.1f, 1000.0f);

	glMatrixMode(GL_MODELVIEW);

	glEnable(GL_CULL_FACE);								//Stop calculation of inside faces
	glEnable(GL_DEPTH_TEST);							//Hidden surface removal

	//clear background colour.
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glEnable(GL_TEXTURE_2D);

	





	//set some parameters so it renders correctly


	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	mCam = new Camera();
	mCam->GetInstance();


	SetUpLevel();


}

//--------------------------------------------------------------------------------------------------

GameScreenLevel1::~GameScreenLevel1()
{
}

//--------------------------------------------------------------------------------------------------


void GameScreenLevel1::SetLight() {
	lighting light = {
		{ 0.2f, 0.2f, 0.2f, 1.0f },
		{ 0.7f, 0.7f, 0.7f, 1.0f },
		{ 0.5f, 0.5f, 0.5f, 1.0f }
	};
	// position of the light in homogeneous coordinates (x, y, z, w)
	// w should be 0 for directional lights, and 1 for spotlights
	float light_pos[] = { 1.0f, 1.0f, 1.0f, 0.0f };

	glLightfv(GL_LIGHT0, GL_AMBIENT, light.ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light.diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light.specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
}

void GameScreenLevel1::SetMaterial()
{
	material material = {
		{ 0.80f, 0.05f, 0.05f, 1.0f },
		{ 0.80f, 0.05f, 0.05f, 1.0f },
		{ 1.0f, 1.0f, 1.0f, 1.0f },
		100.0f
	};

	glMaterialfv(GL_FRONT, GL_AMBIENT, material.ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material.diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material.specular);
	glMaterialf(GL_FRONT, GL_SHININESS, material.shininess);
}


void GameScreenLevel1::Render()
{
	//Clear the screen.
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	for (int i = 0; i < gameObjects.size(); i++)
	{
		gameObjects[i]->Render();
	}
	//Render balls
	for (int j = 0; j < ballgameObjects.size(); j++)
	{
		ballgameObjects[j]->Render();

		//check for ball collision and set new position
		if (ballgameObjects[j]->collided())
		{

			ballgameObjects[j]->Offsetball(charactergameObjects[0]->Getposition());


		}
	}

	//RenderCharacter
	charactergameObjects[0]->Render();

	mCam->Render();

	glColor3f(0.0, 1.0, 0.0);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, 100, 0, 100);
	OutputLine(17, 95, StringStream.str());
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

}

//--------------------------------------------------------------------------------------------------

void GameScreenLevel1::Update(float deltaTime, SDL_Event e)
{




		mCurrentTime += deltaTime;
		cout << mCurrentTime << endl;
		StringStream.str("");
		StringStream.clear();
		StringStream <<"Number Of Hits: "<<mNumOfHits<< "  " << endl << "Time:  " << (int)mCurrentTime << endl;
		
		mNumOfHits = charactergameObjects[0]->numOfHits;
		

		mCam->Update(deltaTime, e);
		charactergameObjects[0]->Update(deltaTime, e);

		for (int i = 0; i < gameObjects.size(); i++) {
			gameObjects[i]->Update(deltaTime);

		}
		//update balls.
		for (int i = 0; i < ballgameObjects.size(); i++) {
			ballgameObjects[i]->Update(deltaTime, charactergameObjects[0]->Getposition());

		}
		//Check for collision between ball and character
		for (int i = 0; i < ballgameObjects.size() - 1; i++) {
			for (int j = i + 1; j < ballgameObjects.size(); j++) {
				Collision::SphereSphereCollision(ballgameObjects[i]->GetBoundingSphere(), charactergameObjects[0]->GetBoundingSphere());
			}
		}

		//increment offset on ball and apply on collision
		for (int j = 0; j < ballgameObjects.size(); j++)
		{
			if (ballgameObjects[j]->collided())
			{
				offset += 5.0;
				ballgameObjects[j]->offset = offset;

			}
		}

		if (charactergameObjects[0]->Win == true)
		{
			SoundManager::GetInstance()->PlayApplause();
			Score::GetInstance()->IncrementValue((100 -(int)mCurrentTime*mNumOfHits));
      		ScreenManager->ChangeScreen(SCREEN_VICTORY);
		}
		else if (charactergameObjects[0]->Dead == true)
		{
			ScreenManager->ChangeScreen(SCREEN_GAMEOVER);
		}

		
		
		
}




void GameScreenLevel1::SetUpLevel()
{


	m_Court = new Object3DS("Court.3ds", "CourtTexture.TGA");
	m_Ball = new Object3DS("Ball.3ds", "BallTexture.TGA");
	m_Denzil = new Object3DS("Denzil.3ds", "DenzilDiffuse.TGA");


	CourtGameObject* courtobj = new CourtGameObject(m_Court, Vector3D(0, 0, 0), Vector3D(0, 0, 0));
	CourtGameObject* courtobj2 = new CourtGameObject(m_Court, Vector3D(40, 0, 0), Vector3D(0, 0, 180));
	DenzilGameObject* denzilobj = new DenzilGameObject(m_Denzil, Vector3D(-15, -13, 0), Vector3D(0, 0, 0));


	for (int i = 0; i < 30; i++)
	{
		BallGameObject* ballobj = new BallGameObject(m_Ball, Vector3D(3, 1, 30), Vector3D(0, 0, 0));
		ballgameObjects.push_back(ballobj);
	}
	gameObjects.push_back(courtobj2);
	gameObjects.push_back(courtobj);
	charactergameObjects.push_back(denzilobj);
}


//--------------------------------------------------------------------------------------------------