#include "GameObject.h"
#include "../gl/glut.h"




GameObject::GameObject(Object3DS* mesh, Vector3D startposition, Vector3D startrotation)
{
	m_meshObject = mesh;
	m_position = startposition;
	m_rotation = startrotation;
}

GameObject::~GameObject()
{
	delete m_meshObject;
	m_meshObject = NULL;
	
}

void GameObject::Render()
{
	glEnable(GL_LIGHTING);

	glPushMatrix();
	glTranslatef(m_position.x, m_position.y, m_position.z);
	glRotatef(m_rotation.y, 0, 1, 0);
	glRotatef(m_rotation.x, 1, 0, 0);
	glRotatef(m_rotation.z, 0, 0, 1);

	m_meshObject->render();

	glPopMatrix();
}

void GameObject::Update(float DeltaTime)
{

}