#ifndef _TARGET_GAMEOBJECT_H
#define _TARGET_GAMEOBJECT_H

#include "Commons.h"
#include "GameObject.h"
class Sphere;
class TargetGameObject : public GameObject
{
public:
	TargetGameObject(Object3DS* mesh, Vector3D startposition, Vector3D startrotation);
	~TargetGameObject();
	Sphere * GetBoundingSphere();
	float offset = 5;
	void Offsetball(Vector3D newpos);
	bool collided();
	bool isoffset = false;
	

	void Update(float deltaTime,Vector3D newpos);
	void Render();

private:
	//Object3DS* m_meshObject;
	Sphere * boundingSphere;
	Vector3D velocity;
	float scale;
	Vector3D acceleration;


};
#endif //_TARGET_GAMEOBJECT_H


