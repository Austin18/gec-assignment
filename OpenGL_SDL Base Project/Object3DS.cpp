#include "object3DS.h"
#include "../gl/glut.h"
#include "3dsLoader.h"
#include "Texture2D.h"

//-------------------------------------------------------------------------------------------

Object3DS::Object3DS(string modelFileName, string textureFilename)
{
	//3ds file to load.
	std::strcpy(fileName, modelFileName.c_str());
	loadModel();
	loadTexture(textureFilename.c_str());
		
}

//------------------------------------------------------


void Object3DS::update(float deltaTime)
{
	//TODO: Move object here
}

void Object3DS:: render()
{
	
	glPushMatrix();
	
	
	
	glBindTexture(GL_TEXTURE_2D, object.id_Texture);
	glBegin(GL_TRIANGLES); // glBegin and glEnd delimit the vertices that define a primitive (in our case triangles) 
	for (int l_index = 0; l_index < object.triangles_qty; l_index++)
	{ 
		//-----------------FIRST VERTEX ----------------// // Texture coordinates of the first vertex
		glTexCoord2f(object.texcoord[object.triangle[l_index].a].u,
			object.texcoord[object.triangle[l_index].a].v);
		// Coordinates of the first vertex 
		glVertex3f(object.vertex[object.triangle[l_index].a].x,
			object.vertex[object.triangle[l_index].a].y,
			object.vertex[object.triangle[l_index].a].z); //Vertex definition
		 //-----------------SECOND VERTEX -----------------
		// Texture coordinates of the second vertex
		glTexCoord2f(object.texcoord[object.triangle[l_index].b].u,
			object.texcoord[object.triangle[l_index].b].v);
		// Coordinates of the second vertex 
		glVertex3f(object.vertex[object.triangle[l_index].b].x,
			object.vertex[object.triangle[l_index].b].y,
			object.vertex[object.triangle[l_index].b].z);
	//-----------------THIRD VERTEX -----------------
		// Texture coordinates of the third vertex 
		glTexCoord2f(object.texcoord[object.triangle[l_index].c].u,
		 object.texcoord[ object.triangle[l_index].c ].v); 
		// Coordinates of the Third vertex
		glVertex3f(object.vertex[object.triangle[l_index].c].x,
			object.vertex[object.triangle[l_index].c].y,
			object.vertex[object.triangle[l_index].c].z); }
	glEnd();
	glPopMatrix();
}

void Object3DS::loadModel()
{
	if (fileName[0] != '---')
		Load3DS(&object, fileName);
}

void Object3DS::loadTexture(const char* filename)
{
	Texture2D* texture = new Texture2D();
	texture->LoadTextureTGA(filename, 1024, 1024);
	object.id_Texture = texture->GetID();
}