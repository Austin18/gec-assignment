#ifndef _PROJECTILE_GAMEOBJECT_H
#define _PROJECTILE_GAMEOBJECT_H

#include "Commons.h"
#include "GameObject.h"

class Sphere;
class ProjectileGameObject : public GameObject
{
public:
	ProjectileGameObject(Object3DS* mesh, Vector3D startposition, Vector3D startrotation);
	~ProjectileGameObject();

	void Update(float deltaTime, Vector3D newpos);
	void Render();
	void setFired();
	Sphere * GetBoundingSphere();
	float offset = 5;
	bool collided();
	bool hasfired = false;
	void Updatehits();
	int numOfHits;

private:
	Object3DS* m_meshObject;
	Sphere * boundingSphere;
	Vector3D velocity;
	float scale;
	Vector3D acceleration;
};




#endif //_PROJECTILE_GAMEOBJECT_H


