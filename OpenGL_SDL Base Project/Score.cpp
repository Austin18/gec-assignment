#include "Score.h"

Score * Score::mInstance = 0;
Score::Score() {
	mValue = 0;
}
Score * Score::GetInstance() {
	if (!mInstance) {
		mInstance = new Score();
	}
	return mInstance;
}


int Score::GetValue() {
	return mValue;
}

void Score::IncrementValue(int amount) {
	mValue += amount;
}
