#ifndef _GAMESCREEN_H
#define _GAMESCREEN_H

#include <SDL.h>
#include <string>
#include <windows.h>
#include <GL\gl.h>
#include <GL\glu.h>
#include "../gl/glut.h"
#include "GameScreenManager.h"


using namespace std;

class GameScreen
{
public:
	GameScreen(GameScreenManager* manager);
	~GameScreen();

	virtual void Render();
	virtual void Update(float deltaTime, SDL_Event e);
	void OutputLine(float x, float y, string text);
	GameScreenManager* ScreenManager;
protected:

};


#endif //_GAMESCREEN_H