#ifndef VICTORY_SCREEN_H
#define VICTORY_SCREEN_H

#include <vector>
#include <string>
#include "GameScreen.h"
#include "GameScreenManager.h"
using namespace std;

class VictoryScreen : public GameScreen {
public:
	VictoryScreen(GameScreenManager* manager);
	~VictoryScreen() {};
	void Render();
	void Update(float deltaTime, SDL_Event e);

protected:

	void HandleInput(SDL_Event e);
	vector<string> mInstructions;



};
#endif //VICTORY_SCREEN_H




