#include "GameScreenManager.h"
#include "GameScreen.h"
#include "GameScreenLevel1.h"
#include "MenuScreen.h"
#include "InstructionsScreen1.h"
#include "GameOver.h"
#include "VictoryScreen.h"
#include "GameScreenLevel2.h"
#include "VictoryScreen2.h"

//--------------------------------------------------------------------------------------------------

GameScreenManager::GameScreenManager(SCREENS startScreen)
{
	mCurrentScreen = NULL;

	//Ensure the first screen is set up.
	ChangeScreen(startScreen);
}

//--------------------------------------------------------------------------------------------------

GameScreenManager::~GameScreenManager()
{
	delete mCurrentScreen;
	mCurrentScreen = NULL;
}

//--------------------------------------------------------------------------------------------------

void GameScreenManager::Render()
{
	mCurrentScreen->Render();
}

//--------------------------------------------------------------------------------------------------

void GameScreenManager::Update(float deltaTime, SDL_Event e)
{
	mCurrentScreen->Update(deltaTime, e);
}

//--------------------------------------------------------------------------------------------------

void GameScreenManager::ChangeScreen(SCREENS newScreen)
{
	//Clear up the old screen.
	if(mCurrentScreen != NULL)
	{
		delete mCurrentScreen;
		
	}

	GameScreenLevel1* tempScreen1;
	MenuScreen* tempScreen2;
	InstructionsScreen1* tempScreen3;
	GameOverScreen* tempScreen4;
	VictoryScreen* tempScreen5;
	GameScreenLevel2* tempScreen6;
	VictoryScreen2* tempScreen7;


	//Initialise the new screen.
	switch(newScreen)
	{
		case SCREEN_INTRO:
			tempScreen3 = new InstructionsScreen1(this);
			mCurrentScreen = (GameScreen*)tempScreen3;
			tempScreen3 = NULL;
		break;

		case SCREEN_MENU:
			tempScreen2 = new MenuScreen(this);
			mCurrentScreen = (GameScreen*)tempScreen2;
			tempScreen2 = NULL;
		break;

		case SCREEN_LEVEL1:
			tempScreen1 = new GameScreenLevel1(this);
			mCurrentScreen = (GameScreen*)tempScreen1;
			tempScreen1 = NULL;
		break;

			
		case SCREEN_GAMEOVER:
			tempScreen4 = new GameOverScreen(this);
			mCurrentScreen = (GameScreen*)tempScreen4;
			tempScreen4 = NULL;
		break;

		case SCREEN_LEVEL2:
			tempScreen6 = new GameScreenLevel2(this);
			mCurrentScreen = (GameScreen*)tempScreen6;
			tempScreen6 = NULL;
			break;
		
		case SCREEN_VICTORY:
			tempScreen5 = new VictoryScreen(this);
			mCurrentScreen = (GameScreen*)tempScreen5;
			tempScreen5 = NULL;
		break;

		case SCREEN_VICTORY2:
			tempScreen7 = new VictoryScreen2(this);
			mCurrentScreen = (GameScreen*)tempScreen7;
			tempScreen7 = NULL;
			break;
		
		default:
		break;
	}
}

//--------------------------------------------------------------------------------------------------