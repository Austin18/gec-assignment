#ifndef _GAMESCREENLEVEL1_H
#define _GAMESCREENLEVEL1_H

#include <SDL.h>
#include "GameScreen.h"
#include "Object3DS.h"
#include "Camera.h"
#include <string>
#include <sstream>
#include <vector>
#include "GameObject.h"
#include "BallGameobject.h"
#include "DenzilGameobject.h"

class GameScreenLevel1 : GameScreen
{
//--------------------------------------------------------------------------------------------------
public:
	GameScreenLevel1(GameScreenManager* manager);
	~GameScreenLevel1();

	void Render();
	void Update(float deltaTime, SDL_Event e);
	void SetLight();
	void SetMaterial();
	void SetUpLevel();
	float Angle;
	float offset = 5.0;
	float mTime;
	stringstream StringStream;
	int mNumOfHits = 0;


	

	

//--------------------------------------------------------------------------------------------------
private:
	
	float mCurrentTime = 0;
	vector<GameObject*> gameObjects;
	vector<BallGameObject*> ballgameObjects;
	vector<DenzilGameObject*>charactergameObjects;
	Object3DS* m_Court;
	Object3DS* m_Ball;
	Object3DS* m_Denzil;
	Camera* mCam;
};


#endif //_GAMESCREENLEVEL1_H