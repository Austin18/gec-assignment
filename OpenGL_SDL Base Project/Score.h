#ifndef SCORE_H
#define SCORE_H



class Score {

private:
	Score();
	int mValue;
	static Score * mInstance;
public:
	static Score * GetInstance();

	int GetValue();

	void IncrementValue(int amount);
};

#endif // SCORE_H
