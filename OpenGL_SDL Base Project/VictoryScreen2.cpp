#include "VictoryScreen2.h"
#include "GameScreenManager.h"
#include "Score.h"
#include <string>

VictoryScreen2::VictoryScreen2(GameScreenManager* manager) : GameScreen(manager) {

	Score::GetInstance()->GetValue();
	mInstructions.push_back("VICTORY Your Final Score is ");
	mInstructions.push_back(to_string(Score::GetInstance()->GetValue()));
	mInstructions.push_back("Hit enter to Return to menu");


}

void VictoryScreen2::Render() {
	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, 100, 0, 100);
	//Clear the screen.
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	for (int j = 0; j < mInstructions.size(); j++) {
		glColor3f(0.0, 0.0, 0.0);
		OutputLine(20, 70 - 10 * j - 10, mInstructions[j]);
	}

	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}

void VictoryScreen2::Update(float deltaTime, SDL_Event e) {
	HandleInput(e);
}

void VictoryScreen2::HandleInput(SDL_Event e) {
	switch (e.type)
	{
	case SDL_KEYUP:
		switch (e.key.keysym.sym) {
		case SDLK_RETURN:
			ScreenManager->ChangeScreen(SCREEN_MENU);
			break;
		}
	}
}



