#ifndef _COURT_GAMEOBJECT_H
#define _COURT_GAMEOBJECT_H

#include "Commons.h"
#include "GameObject.h"

class CourtGameObject : public GameObject
{
public:
	CourtGameObject(Object3DS* mesh, Vector3D startposition, Vector3D startrotation);
	~CourtGameObject();

	void Update();
	void Render();




};
#endif //_COURT_GAMEOBJECT_H

