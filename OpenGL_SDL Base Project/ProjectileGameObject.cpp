#include "ProjectileGameobject.h"
#include <iostream>
#include "../gl/glut.h"
#include "Collisions.h"
#include "DenzilGameobject.h"
#include <Windows.h>
#include "SoundManager.h"
using namespace std;

ProjectileGameObject::ProjectileGameObject(Object3DS* mesh, Vector3D startposition, Vector3D startrotation) : GameObject(mesh, startposition, startrotation)
{
	m_position.x = startposition.x;
	m_position.y = startposition.y;
	m_position.z = startposition.z;

	//float x = startposition.x;
	m_meshObject = mesh;

	numOfHits = 0;
	scale = 0.7;
	boundingSphere = new Sphere(m_position, scale*1.4);

	velocity.x = 0;
	velocity.y = 50;
	velocity.z = 0;

	acceleration.x = 0;
	acceleration.y = 20;
	acceleration.z = 0;


	cout << velocity.x << "," << velocity.y << "," << velocity.z << endl;
}

ProjectileGameObject::~ProjectileGameObject()
{}

void ProjectileGameObject::Render()
{
	if (m_meshObject != nullptr)
	{

		GameObject::Render();


	}
	else
	{
		glDisable(GL_LIGHTING);
		glBindTexture(GL_TEXTURE_2D, 0);
		glColor3f(1.0f, 0.5f, 0.0f);
		glPushMatrix();
		glTranslatef(m_position.x, m_position.y, m_position.z);
		glutSolidSphere(.5f, 10, 10);
		glPopMatrix();
	}
}

void ProjectileGameObject::Update(float DeltaTime, Vector3D newpos)
{
	
	
	if (hasfired)
	{
		m_position += velocity*DeltaTime + acceleration * (0.5 * DeltaTime * DeltaTime);
		velocity += acceleration * DeltaTime;

		//velocity *= 0.98;



		if (m_position.y > 40)
		{
 			hasfired = false;
		}
		if (boundingSphere->GetCollided())
		{
			hasfired = false;
			Updatehits();
		}

		
	}
	else {

		m_position.x = newpos.x;
		m_position.y = newpos.y;
		m_position.z = newpos.z;
		


	}
	boundingSphere->SetCollided(false);
	boundingSphere->Update(m_position);


}

Sphere * ProjectileGameObject::GetBoundingSphere()
{
	return boundingSphere;
}

void ProjectileGameObject::setFired()
{
	hasfired = true;

}



bool ProjectileGameObject::collided()
{
	if (boundingSphere->GetCollided())
	{
		return true;
	}


}

void ProjectileGameObject::Updatehits()
{
	numOfHits++;
}


