#ifndef INSTRUCTIONS_SCREEN1_H
#define INSTRUCTIONS_SCREEN1_H

#include <vector>
#include <string>
#include "GameScreen.h"
#include "GameScreenManager.h"
using namespace std;

class InstructionsScreen1 : public GameScreen {
public:
	InstructionsScreen1(GameScreenManager* manager);
	~InstructionsScreen1() {};
	void Render();
	void Update(float deltaTime, SDL_Event e);

protected:

	void HandleInput(SDL_Event e);
	vector<string> mInstructions;
	

	
};
#endif //INSTRUCTIONS_SCREEN1_H


